# Importar las bibliotecas necesarias
import json
import os
import pandas as pd
import numpy as np
import dash

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, State
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input, Output
from flask_caching import Cache

# Configuración de los archivos de datos según el entorno
if os.environ.get("ENV") == "production":
    dsFile = "/home/rigomx/dashboard-ds24/data/conjunto_de_datos_iter_00CSV20.csv"
    geojsonFile = "/home/rigomx/dashboard-ds24/data/geo/mexicoHigh.json"
else:
    dsFile = "data/conjunto_de_datos_iter_00CSV20.csv"
    geojsonFile = "data/geo/mexicoHigh.json"
    
# Configuración de la aplicación Dash
app = dash.Dash(__name__)
app.title = "Población en México"

# Configuración de la caché
cache = Cache(app.server, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory'
})

# Función para cargar los datos
@cache.memoize()
def cargar_datos():
    df = pd.read_csv(dsFile, low_memory=False)
    with open(geojsonFile) as f:
        geojson = json.load(f)
    return df, geojson

# Cargamos los datos
df, geojson = cargar_datos()

# Definimos las columnas que contienen los datos de interés
columna_estado = "NOM_ENT"
columna_poblacion = "POBTOT"
educacion_basica = "P15PRI_CO"
educacion_media = "P15SEC_CO"
educacion_superior = "P18YM_PB"

# Crea un diccionario para mapear los nombres de los estados en los datos a los nombres de los estados en el GeoJSON
mapeo_nombres_estados = {
    "Coahuila de Zaragoza": "Coahuila",
    "Michoacán de Ocampo": "Michoacán",
    "Veracruz de Ignacio de la Llave": "Veracruz",
}

df["NOM_ENT"] = df["NOM_ENT"].replace(mapeo_nombres_estados)

# Limpieza de los datos
df = df[df["NOM_ENT"] != "Total nacional"]
df = df[~df["NOM_MUN"].str.startswith("Total de la entidad")]

lista_eliminar = [
    "Total nacional",
    "Localidades de una vivienda",
    "Localidades de dos viviendas",
    "Total de la Entidad",
    "Total del Municipio",
]
df = df[~df["NOM_LOC"].isin(lista_eliminar)]
df = df.dropna(subset=[columna_poblacion])
df = df[df[columna_poblacion] != "*"]

# Agrupación y ordenamiento de los datos por estado
poblacion_estado = df.groupby(columna_estado)[columna_poblacion].sum().reset_index()
df_poblacion_agrupado_ = df.groupby(columna_estado)[columna_poblacion].sum().reset_index()
df_poblacion_agrupado_ordenado = df_poblacion_agrupado_.sort_values(
    by=columna_poblacion, ascending=False
)

# Creación de la figura del mapa de población
fig_map_poblacion = px.choropleth(
    df_poblacion_agrupado_ordenado,
    geojson=geojson,
    color=columna_poblacion,
    locations=columna_estado,
    featureidkey="properties.name",
    projection="natural earth",
    labels={"POBTOT": "Población Total", "NOM_ENT": "Estado"},
    color_continuous_scale="blues",
)
fig_map_poblacion.update_geos(
    showcountries=False,
    showcoastlines=True,
    showland=True,
    fitbounds="locations",
    landcolor="lightgray",
    visible=False,
)
fig_map_poblacion.update_layout(
    plot_bgcolor="rgba(0,0,0,0)",
    margin={"r": 0, "t": 0, "l": 0, "b": 0},
    title_text="Población de México por estado",
)

# Creación de la figura de las barras de población
df_top_10_estados_poblacion = df_poblacion_agrupado_ordenado.head(10)
fig_barras_ordenado_poblacion = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados_poblacion["NOM_ENT"][::-1],
            x=df_top_10_estados_poblacion["POBTOT"][::-1],
            orientation="h",
            marker=dict(color="blue"),
        )
    ]
)
fig_barras_ordenado_poblacion.update_layout(
    title_text="Top 10 estados de México con mayor población",
    xaxis_title="Proporción de personas con educación básica",
    yaxis_title="Estado",
)

# Limpieza y conversión de los datos de educación
df = df.dropna(subset=[columna_poblacion])
df[columna_poblacion] = pd.to_numeric(df[columna_poblacion], errors="coerce")
df = df.dropna(subset=[educacion_basica])
df[educacion_basica] = pd.to_numeric(df[educacion_basica], errors="coerce")
df = df.dropna(subset=[educacion_media])
df[educacion_media] = pd.to_numeric(df[educacion_media], errors="coerce")
df = df.dropna(subset=[educacion_superior])
df[educacion_superior] = pd.to_numeric(df[educacion_superior], errors="coerce")

# Cálculo de las proporciones de educación
df["prop_basica"] = df[educacion_basica] / df["POBTOT"]
df["prop_media"] = df[educacion_media] / df["POBTOT"]
df["prop_superior"] = df[educacion_superior] / df["POBTOT"]
df = df.dropna(subset=["prop_basica"])
df = df.dropna(subset=["prop_media"])
df = df.dropna(subset=["prop_superior"])

# Tratamiento de outliers en la proporción de educación básica
media_ebasica = df["prop_basica"].mean()
std_ebasica = df["prop_basica"].std() # Desviación estándar
outlier_threshold_basica = media_ebasica + 3 * std_ebasica # Umbral de outliers
median_ebasica = df["prop_basica"].median()
df["prop_basica"] = df["prop_basica"].where(
    df["prop_basica"] <= outlier_threshold_basica, median_ebasica
)

# Agrupación y ordenamiento de los datos de educación básica
df_agrupado_basica = df.groupby("NOM_ENT")["prop_basica"].mean().reset_index()
df_agrupado_ordenado_basica = df_agrupado_basica.sort_values(
    by="prop_basica", ascending=False
)

# Creación de la figura del mapa de educación básica
fig_basica = px.choropleth(
    df_agrupado_ordenado_basica,
    geojson=geojson,
    color="prop_basica",
    locations="NOM_ENT",
    featureidkey="properties.name",
    projection="natural earth",
    labels={"NOM_ENT": "Estado", "prop_basica": "Media de educación básica"},
    color_continuous_scale="blues",
)
fig_basica.update_geos(fitbounds="locations", visible=False)
fig_basica.update_layout(
    title_text="Proporción de personas con educación básica (Ordenado por estado)",
    title_x=0.5,
)

# Creación de la figura de las barras de educación básica
df_top_10_estados = df_agrupado_ordenado_basica.head(10)
fig_barras_basica = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados["NOM_ENT"][::-1],
            x=df_top_10_estados["prop_basica"][::-1],
            orientation="h",
            marker=dict(color="blue"),
        )
    ]
)
fig_barras_basica.update_layout(
    title_text="Top 10 estados con educación básica",
    xaxis_title="Proporción de personas con educación básica",
    yaxis_title="Estado",
)

# Tratamiento de outliers en la proporción de educación media
media_emedia = df["prop_media"].mean()
std_emedia = df["prop_media"].std() # Desviación estándar
outlier_threshold_media = media_emedia + 3 * std_emedia # Umbral de outliers
median_emedia = df["prop_media"].median()
df["prop_media"] = df["prop_media"].where(
    df["prop_media"] <= outlier_threshold_media, median_emedia
)

# Agrupación y ordenamiento de los datos de educación media
df_agrupado_media = df.groupby("NOM_ENT")["prop_media"].mean().reset_index()
df_agrupado_ordenado_media = df_agrupado_media.sort_values(
    by="prop_media", ascending=False
)

# Creación de la figura del mapa de educación media
fig_media = px.choropleth(
    df_agrupado_ordenado_media,
    geojson=geojson,
    color="prop_media",
    locations=columna_estado,
    featureidkey="properties.name",
    projection="natural earth",
    labels={"NOM_ENT": "Estado", "prop_media": "Media de educación media"},
    color_continuous_scale="greens",
)
fig_media.update_geos(fitbounds="locations", visible=False)
fig_media.update_layout(
    title_text="Proporción de personas con educación media",
    margin={"r": 0, "t": 0, "l": 0, "b": 0},
)

# Creación de la figura de las barras de educación media
df_top_10_estados_media = df_agrupado_ordenado_media.head(10)
fig_barras_media = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados_media["NOM_ENT"][::-1],
            x=df_top_10_estados_media["prop_media"][::-1],
            orientation="h",
            marker=dict(color="green"),
        )
    ]
)
fig_barras_media.update_layout(
    title_text="Top 10 estados con educación media",
    xaxis_title="Proporción de personas con educación media",
    yaxis_title="Estado",
)

# Tratamiento de outliers en la proporción de educación superior
media_esuperior = df["prop_superior"].mean()
std_esuperior = df["prop_superior"].std() # Desviación estándar
outlier_threshold_superior = media_esuperior + 3 * std_esuperior # Umbral de outliers
median_esuperior = df["prop_superior"].median()
df["prop_superior"] = df["prop_superior"].where(
    df["prop_superior"] <= outlier_threshold_superior, median_esuperior
)

# Creación de la figura de las barras de educación superior
df_agrupado_superior = df.groupby("NOM_ENT")["prop_superior"].mean().reset_index()
df_agrupado_ordenado_superior = df_agrupado_superior.sort_values(
    by="prop_superior", ascending=False
)

# Creación de la figura del mapa de educación superior
fig_superior = px.choropleth(
    df_agrupado_ordenado_superior,
    geojson=geojson,
    color="prop_superior",
    locations=columna_estado,
    featureidkey="properties.name",
    projection="natural earth",
    labels={"NOM_ENT": "Estado", "prop_superior": "Media de educación superior"},
    color_continuous_scale="purples",
)
fig_superior.update_geos(fitbounds="locations", visible=False)
fig_superior.update_layout(
    margin={"r": 0, "t": 0, "l": 0, "b": 0},
    title_text="Proporción de personas con educación superior",
)

# Creación de la figura de las barras de educación superior
df_top_10_estados_superior = df_agrupado_ordenado_superior.head(10)
fig_barras_superior = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados_superior["NOM_ENT"][::-1],
            x=df_top_10_estados_superior["prop_superior"][::-1],
            orientation="h",
            marker=dict(color="purple"),
        )
    ]
)
fig_barras_superior.update_layout(
    title_text="Top 10 estados con educación superior",
    xaxis_title="Proporción de personas con educación superior",
    yaxis_title="Estado",
)

#  Tratamiento de outliers en la proporción de educación superior
df["POBMAS"] = pd.to_numeric(df["POBMAS"].replace("*", np.nan))
df["POBFEM"] = pd.to_numeric(df["POBFEM"].replace("*", np.nan))
df["POBTOT"] = pd.to_numeric(df["POBTOT"].replace("*", np.nan))

total_hombres_por_estado = df.groupby("NOM_ENT")["POBMAS"].sum()
total_mujeres_por_estado = df.groupby("NOM_ENT")["POBFEM"].sum()
total_poblacion_por_estado = df.groupby("NOM_ENT")["POBTOT"].sum()

total_poblacion_por_sexo_estado = pd.DataFrame(
    {
        "POBMAS": total_hombres_por_estado,
        "POBFEM": total_mujeres_por_estado,
        "POBTOT": total_poblacion_por_estado,
    }
)

total_poblacion_por_sexo_estado = total_poblacion_por_sexo_estado[
    ["POBFEM", "POBMAS", "POBTOT"]
]

# Ordenamiento de la población por sexo
poblacion_masculina_ordenado = total_poblacion_por_sexo_estado["POBMAS"].sort_values(
    ascending=False
)
poblacion_femenina_ordenado = total_poblacion_por_sexo_estado["POBFEM"].sort_values(
    ascending=False
)

total_poblacion_por_sexo_estado["PROMFEM"] = (
    total_poblacion_por_sexo_estado["POBFEM"]
    / total_poblacion_por_sexo_estado["POBTOT"]
) * 100
total_poblacion_por_sexo_estado["PROMMAS"] = (
    total_poblacion_por_sexo_estado["POBMAS"]
    / total_poblacion_por_sexo_estado["POBTOT"]
) * 100

#  Mapa de la proporción de hombres
fig_mapa_hombres = px.choropleth(
    total_poblacion_por_sexo_estado,
    geojson=geojson,
    color="POBMAS",
    locations=total_poblacion_por_sexo_estado.index,
    featureidkey="properties.name",
    projection="natural earth",
    labels={"NOM_ENT": "Estado", "POBMAS": "Población de hombres"},
    color_continuous_scale="blues",
)

fig_mapa_hombres.update_geos(fitbounds="locations", visible=False)
fig_mapa_hombres.update_layout(
    margin={"r": 0, "t": 0, "l": 0, "b": 0}, title_text="Proporción de hombres"
)

df_top_10_estados_superior = poblacion_masculina_ordenado.head(10).sort_values(
    ascending=False
)

# Grafica de barras de hombres
fig_barras_masculina = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados_superior.index[::-1],
            x=df_top_10_estados_superior[::-1],
            orientation="h",
            marker=dict(color="blue"),
        )
    ]
)

fig_barras_masculina.update_layout(
    title_text="Top 10 estados con mayor proporción de hombres",
    xaxis_title="Proporción de hombres",
    yaxis_title="Estado",
)

# Mapa de mujeres
fig_mapa_mujeres = px.choropleth(
    poblacion_femenina_ordenado,
    geojson=geojson,
    color="POBFEM",
    locations=total_poblacion_por_sexo_estado.index,
    featureidkey="properties.name",
    projection="natural earth",
    labels={"locations": "Estado", "POBFEM": "Población de mujeres"},
    color_continuous_scale="pinkyl",
)
fig_mapa_mujeres.update_geos(fitbounds="locations", visible=False)
fig_mapa_mujeres.update_layout(
    margin={"r": 0, "t": 0, "l": 0, "b": 0}, title_text="Proporción de mujeres"
)

df_top_10_estados_superior_femenina = poblacion_femenina_ordenado.head(10).sort_values(
    ascending=False
)

# Grafica de barras de mujeres
fig_barras_femenina = go.Figure(
    data=[
        go.Bar(
            y=df_top_10_estados_superior_femenina.index[::-1],
            x=df_top_10_estados_superior_femenina[::-1],
            orientation="h",
            marker=dict(color="pink"),
        )
    ]
)

fig_barras_femenina.update_layout(
    title_text="Top 10 estados con mayor poblacion femenina",
    xaxis_title="Proporción de poblacion femenina",
    yaxis_title="Estado",
)

# Grafico piramide poblacional
fig_mas_fem = px.choropleth(
    total_poblacion_por_sexo_estado,
    geojson=geojson,
    color="PROMFEM",
    locations=total_poblacion_por_sexo_estado.index,
    featureidkey="properties.name",
    projection="natural earth",
    color_continuous_scale=[
        (0.0, "blue"),
        (0.5, "blue"),
        (0.5, "pink"),
        (1.0, "pink"),
    ],  # Definir la escala de colores
    range_color=(0, 100),
    labels={
        "POBTOT": "Población Total",
        "POBMAS": "Población Masculina",
        "POBFEM": "Población Femenina",
        "PROMFEM": "Porcentaje de Población Femenina (%)",
        "PROMMAS": "Porcentaje de Población Masculina (%)",
    },
    hover_data=["POBTOT", "POBMAS", "POBFEM", "PROMFEM", "PROMMAS"],
    title="Porcentaje de Población por Estado",
)

fig_mas_fem.update_geos(
    showcountries=False,
    showcoastlines=True,
    showland=True,
    fitbounds="locations",
    landcolor="lightgray",
    visible=False,
)
fig_mas_fem.update_layout(plot_bgcolor="rgba(0,0,0,0)", margin={"r": 0, "t": 0, "l": 0, "b": 0})

# Grafico piramide poblacional
fig_mas_fem_pyramid = go.Figure()

fig_mas_fem_pyramid.add_trace(
    go.Bar(
        x=total_poblacion_por_sexo_estado["POBFEM"]
        .head(10)
        .sort_values(ascending=False),
        y=poblacion_masculina_ordenado.index,
        orientation="h",
        name="Mujeres",
        marker=dict(color="pink"),
    )
)

fig_mas_fem_pyramid.add_trace(
    go.Bar(
        x=-total_poblacion_por_sexo_estado["POBMAS"]
        .head(10)
        .sort_values(ascending=False),
        y=poblacion_masculina_ordenado.index,
        orientation="h",
        name="Hombres",
        marker=dict(color="blue"),
    )
)

fig_mas_fem_pyramid.update_layout(
    title="Población por Estado (Ambos Sexos)",
    barmode="overlay",
    bargap=0.1,
    yaxis=dict(title="Estado"),
    xaxis=dict(title="Población"),
    showlegend=True,
    legend=dict(x=0.05, y=1),
    plot_bgcolor="rgba(0,0,0,0)",
    margin={"r": 20, "t": 50, "l": 20, "b": 50},
)

figures_dict = {
    "Población Total": (fig_map_poblacion, fig_barras_ordenado_poblacion),
    "Educación Básica": (fig_basica, fig_barras_basica),
    "Educación Media": (fig_media, fig_barras_media),
    "Educación Superior": (fig_superior, fig_barras_superior),
    "Piramides de Población": (fig_mas_fem, fig_mas_fem_pyramid),
    "Proporción de Hombres": (fig_mapa_hombres, fig_barras_masculina),
    "Proporción de Mujeres": (fig_mapa_mujeres, fig_barras_femenina),
}

# Definición de la interfaz de usuario de la aplicación
app.layout = html.Div(
    id="root",
    children=[
        html.Div(
            id="header",
            children=[
                html.H4(children="Informacion sobre la población en México por estado"),
                html.P(
                    id="description",
                    children="Este mapa muestra Informacion variada sobre la poblacion de México.",
                ),
            ],
        ),
        html.Div(
            id="app-container",
            children=[
                html.Div(
                    id="left-column",
                    children=[
                        html.Div(
                            id="dropdown-container",
                            children=[
                                html.H4(
                                    "Selecciona el gráfico: ",
                                    style={"font-size": "24px"},
                                ),
                                dcc.Dropdown(
                                    options=[
                                        {"label": i, "value": i}
                                        for i in figures_dict.keys()
                                    ],
                                    value="Población Total",
                                    id="dropdown",
                                ),
                            ],
                        ),
                        html.Div(
                            id="heatmap-container",
                            children=[
                                html.P(
                                    "Mapa de Pobación",
                                    id="heatmap-title",
                                ),
                                dcc.Graph(
                                    id="county-choropleth",
                                ),
                            ],
                        ),
                    ],
                ),
                html.Div(
                    id="graph-container",
                    children=[
                        dcc.Graph(
                            id="bar-chart",
                        ),
                        html.P([
                            'En el sitio del INEGI se encuentran los resultados del CENSO 2020 para cada una de las entidades de la población ',
                            html.A('aquí', href='https://www.inegi.org.mx/programas/ccpv/2020/default.html#Datos_abiertos', target='_blank'),
                        ]),
                        html.P([
                            'Este dashboard fue basado en el trabajo de [Dash Opioid Epidemic]. ',
                            html.A('Click aqui', href='https://dash-gallery.plotly.host/dash-opioid-epidemic/', target='_blank'),
                            ' para más detalles.'
                        ]),
                        html.P([
                            'Dashboard de practica para el modulo Data Visualization. ',
                            html.A('Aqui', href='#', target='_blank'),
                            ' aqui se encuentra el repositorio del dashboard.'
                        ])
                    ],
                ),
            ],
        ),
    ],
)

# Definición de las funciones de respuesta a la interacción del usuario
@app.callback(
    [Output("county-choropleth", "figure"), Output("bar-chart", "figure")],
    Input("dropdown", "value"),
)
def update_graph(selected_dropdown_value):
    return figures_dict[selected_dropdown_value]

# Arrancar la aplicación
if __name__ == "__main__":
    app.run_server(debug=True)
