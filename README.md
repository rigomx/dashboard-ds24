# Dashboard de visualización de datos de la población de México

Este proyecto tiene como objetivo visualizar los datos de la población de México a nivel estatal. Utilizamos Dash de Plotly para crear un dashboard interactivo, que incluye un mapa de calor y gráficos de barras.

## Características

- Mapa de calor con la población de México a nivel estatal.
- Gráficos de barras que muestran la población por estado y municipio.
- Selector de estado y municipio para personalizar los gráficos de barras.

## Cómo correr el proyecto

1. Clona este repositorio.
2. Instala las dependencias con `pip install -r requirements.txt`.
3. Corre el archivo `main.py` con `python main.py`.
4. Abre un navegador web y ve a `localhost:8050`.

### Datos

Los datos de la población provienen del [INEGI](https://www.inegi.org.mx/). Los datos geográficos provienen de un archivo GeoJSON de los municipios de México.

### Dashboard

Este dashboard fue basado en el trabajo de [Dash Opioid Epidemic](https://dash-gallery.plotly.host/dash-opioid-epidemic/).
